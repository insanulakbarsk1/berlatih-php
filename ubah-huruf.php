<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Huruf</title>
</head>
<body>
	<?php
	function ubah_huruf($string) 
	{
		$huruf = "abcdefghijklmnopqrstuvwxyz";
		$hasil = "";
		for ($i = 0; $i < strlen($string); $i++) {
			if ($string[$i] == "z") {
				$hasil .= "a";
			} else {
				for ($j = 0; $j < strlen($huruf); $j++) {
					if ($string[$i] == $huruf[$j]) {
						$hasil .= $huruf[$j+1];
					}
				}
			}
		}
	return $hasil."<br>";
	}

	// TEST CASES
	echo ubah_huruf('wow'); // xpx
	echo ubah_huruf('developer'); // efwfmpqfs
	echo ubah_huruf('laravel'); // mbsbwfm
	echo ubah_huruf('keren'); // lfsfo
	echo ubah_huruf('semangat'); // tfnbohbu
		?>
</body>
</html>